---
date: '2012-10-21 14:00:00'
layout: default
slug: hello-world
status: publish
title: Hello {{ world }}!
---

{{ page.title }}
================

Welcome to my new blog! As first post I will present my setup.

### Generation of HTML pages

<pre class="code fleft">
~ tree
.
├── _config.yml
├── _includes
├── _layouts
│   └── default.html
├── _posts
│   └── 2012-10-21-hello-world.md
├── _site
│   ├── 2012
│   │   └── 10
│   │       └── 21
│   │           └── hello-world.html
│   ├── index.html
│   └── server.sh
├── index.html
└── server.sh
</pre>

This blog post content is structured using the [Markdown](http://daringfireball.net/projects/markdown/basics "Markdown syntax") syntax. This syntax allows you for example, to express that some text is a title, that another one is a link and so on. Of course a weblog doesn't serve markdown documents but instead HTML. To achieve this I'm using a tool called [Jekyll](http://jekyllrb.com/ "Jekyll"). Jekyll is a static blog generator.

Basically, you define :

* the global appearance of your blog (titles, header, footer navigation panel, style, …) in layout files that must be placed in __\_layouts__ directory.
* some blog posts written using your favorite syntax (markdown, textile, …) in __\_posts__ directory.

&nbsp;

&nbsp;

<pre class="code fright">
&#123;% for post in site.posts limit: 10 %&#125;
&lt;li&gt;
	&lt;a href="&#123;&#123; post.url &#125;&#125;"&gt;&#123;&#123; post.title &#125;&#125;&lt;/a&gt;
&lt;/li&gt;
&#123;% endfor %&#125;
</pre>

You can iterate over your post set using _site.posts_. A post has various properties, _url_ and a _title_ are one them. 

### Serving the pages

As you probably expect the setup uses a web server for serving the pages. Namely, nginx.

### Deployment of blog updates

<pre class="code fleft">
#! /usr/bin/env sh
	
PORT=8080

nc -w 1 -l $PORT | while read l; do
	echo $l;
done
	
git pull
jekyll
./server.sh &amp;
</pre>

I'm using [Bitbucket](https://bitbucket.org/ "Bitbucket") to host the git repository containing the files I described above. My blog is hosted by an [EC2](http://aws.amazon.com/ec2/ "EC2") instance.

In order to regenerate my blog each time an update is pushed to the git repository I use Bitbucket POST Service. Bitbucket [POST Service](https://confluence.atlassian.com/display/BITBUCKET/POST+Service+Management
 "POST Service") feature allows me to define a resource on which an HTTP POST request will be executed to notify any changes occuring on the repository. This allows me to trigger any code I want.

The presented shell script waits for incoming connections (special thanks to netcat) to a given port, retrieves changes from the blog repository, generates html pages and reloads itself.