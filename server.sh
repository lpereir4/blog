#! /usr/bin/env sh

PORT=8080

nc -w 1 -l $PORT | while read l; do
  echo $l;
done

git pull
jekyll
./server.sh &